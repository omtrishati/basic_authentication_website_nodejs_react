import React from 'react';
import { render } from 'react-dom';

import { App } from './App';

// setup fake backend

/*Uncomment below two lines if you don't have backend server running*/
// import { configureFakeBackend } from './_helpers';
// configureFakeBackend();

render(
    <App />,
    document.getElementById('app')
);